package uz.pdp.appmultisqldb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMultiSqlDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppMultiSqlDbApplication.class, args);
    }

}
