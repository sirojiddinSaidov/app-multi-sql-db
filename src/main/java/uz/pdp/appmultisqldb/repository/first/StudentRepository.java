package uz.pdp.appmultisqldb.repository.first;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appmultisqldb.entity.first.Student;

@RepositoryRestResource(path = "student")
public interface StudentRepository extends JpaRepository<Student, Integer> {
}