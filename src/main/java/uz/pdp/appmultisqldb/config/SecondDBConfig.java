package uz.pdp.appmultisqldb.config;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "uz.pdp.appmultisqldb.repository.second",
        entityManagerFactoryRef = "secondEntityManagerFactory",
        transactionManagerRef = "secondTransactionManager"
)
public class SecondDBConfig {

    @Value("${spring.datasource.ikki.username}")
    private String username;

    @Value("${spring.datasource.ikki.password}")
    private String password;

    @Value("${spring.datasource.ikki.url}")
    private String url;

    @Value("${spring.datasource.ikki.ddlMode}")
    private String ddlMode;

    @Bean
    public DataSource secondDataSource() {
        return new DriverManagerDataSource(url, username, password);
    }


    @Bean(name = "secondEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean carEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", ddlMode);
        properties.put("hibernate.show_sql", true);
        return builder
                .dataSource(secondDataSource())
                .packages("uz.pdp.appmultisqldb.entity.second")
                .properties(properties)
                .build();
    }


    @Bean(name = "secondTransactionManager")
    public PlatformTransactionManager carTransactionManager(@Qualifier("secondEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

}
