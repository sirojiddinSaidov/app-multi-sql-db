package uz.pdp.appmultisqldb.config;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "uz.pdp.appmultisqldb.repository.third",
        entityManagerFactoryRef = "thirdEntityManagerFactory",
        transactionManagerRef = "thirdTransactionManager"
)
public class ThirdDBConfig {

    @Value("${spring.datasource.uch.username}")
    private String username;

    @Value("${spring.datasource.uch.password}")
    private String password;

    @Value("${spring.datasource.uch.url}")
    private String url;

    @Value("${spring.datasource.uch.ddlMode}")
    private String ddlMode;

    @Bean
    public DataSource thirdDataSource() {
        return new DriverManagerDataSource(url, username, password);
    }


    @Bean(name = "thirdEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean carEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", ddlMode);
        properties.put("hibernate.show_sql", true);
        return builder
                .dataSource(thirdDataSource())
                .packages("uz.pdp.appmultisqldb.entity.third")
                .properties(properties)
                .build();
    }


    @Bean(name = "thirdTransactionManager")
    public PlatformTransactionManager carTransactionManager(@Qualifier("thirdEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

}
